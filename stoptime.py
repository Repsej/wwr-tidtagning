from datetime import datetime


def stopTime():
    print("Enter the start numbers when you want to generate a end time")
    print("You can enter multiple timestamps by having spaces between the numbers and they have to be 3 digits long")
    print("Type 'exit' when you want to quit the program")
    entries = 0
    with open('sluttid.txt', 'w') as f:
        while True:
            start_numbers = input("start numbers: ")
            if start_numbers == "exit":
                break
            start_number_list = start_numbers.split()
            newList = []
            for start_number in start_number_list:
                try:
                    int(start_number)
                except ValueError or TypeError:
                    print(start_number + " is not a number, removing...")
                    continue
                if len(start_number) != 3:
                    print(start_number + " is not 3 numbers long, removing...")
                    continue
                newList.append(start_number)
            if len(newList) > 0:
                input("Press enter when you want to enter the time")
            for start_number in newList:
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                print(start_number + ": " + current_time)
                f.write(start_number + ": " + current_time + '\n')
                entries += 1
        f.close()
    return entries


entries = stopTime()
print("-----------------------------------")
print("Program stopped. Saved " + str(entries) + " timestamps")
