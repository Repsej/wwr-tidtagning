# WWR Tidtagning



## Getting started

Start with downloading python if it does not already exist on the device [here](https://www.python.org/downloads/).

Then either download the project or clone it using the `Clone with HTTPS url`. In windows 11 all you need to do is open a terminal (cmd) and write `git clone https://gitlab.com/Repsej/wwr-tidtagning.git` and the project should be downloaded.

![description](img.png)

If you haven't already, open a terminal (cmd) and simply run `python .\stoptime.py` inside the `wwr-tidtagning` folder.

The program will save the times in the `sluttid.txt` file and will be overwritten everytime you run the program.